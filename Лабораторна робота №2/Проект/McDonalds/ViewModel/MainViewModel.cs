﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using ViewModel.Annotations;

namespace ViewModel
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private MainViewNavigationListener _listener;
        private MainDishes _mainDish;
        private string _categoryDish;

        public MainViewModel(MainViewNavigationListener listener)
        {
            _listener = listener;

            FirstName = "First Name";
            LastName = "Last Name";
            HowManyFood = "0";
            Price = "$$$ Price $$$";
            
            SelectedCategoryDish = "Category Dishes";

            MainDishesObservableCollection = new ObservableCollection<string>
            {
                MainDishes.DRINKS.ToString(),
                MainDishes.BURGERS.ToString(),
                MainDishes.MENUS.ToString()
            };

            LogoutCommand = new Command((o => _listener.OnOpenLoginViewListener()));
        }

        public ObservableCollection<string> MainDishesObservableCollection { get; set; }
        public ObservableCollection<string> CategoryDishesObservableCollection { get; set; }

        public ICommand LogoutCommand { get; }
        public ICommand OrderCommand { get; }
        public ICommand SaveCommand { get; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string HowManyFood { get; set; }
        public string Price { get; set; }

        public MainDishes SelectedMainDish
        {
            get { return _mainDish;}
            set
            {
                _mainDish = value;
                SetCategoryDishes();
            } }

        public string SelectedCategoryDish { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        
        private void SetCategoryDishes()
        {
            switch (SelectedMainDish)
            {
                case MainDishes.DRINKS:
                {
                    CategoryDishesObservableCollection = new ObservableCollection<string>
                    {
                        "Cola",
                        "Fanta",
                        "Tea",
                        "Coffee"
                    };
                    break;
                }
                case MainDishes.BURGERS:
                {
                    CategoryDishesObservableCollection = new ObservableCollection<string>
                    {
                        "Big Mac",
                        "Big Tasty",
                        "Cheeseburger",
                        "Double Cheeseburger"
                    };
                        break;
                }
                case MainDishes.MENUS:
                {
                    CategoryDishesObservableCollection = new ObservableCollection<string>
                    {
                        "Big Mac Menu",
                        "Happy meal",
                        "Mix Box"
                    };
                        break;
                }
            }

            OnPropertyChanged(nameof(CategoryDishesObservableCollection));
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public enum MainDishes
    {
        DRINKS,
        BURGERS,
        MENUS
    }

    public interface MainViewNavigationListener
    {
        void OnOpenLoginViewListener();
    }
}
