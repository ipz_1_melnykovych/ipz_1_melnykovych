﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;

namespace ViewModel
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        private string _error;
        private OnOpenMainView _listener;

        public LoginViewModel(OnOpenMainView listener)
        {
            this._listener = listener;
            LoginCommand = new Command((x) =>
            {
                if (UserName == "1111" && Password == "1111")
                {
                    Error = "";
                    listener.openMainView();
                }
                else
                {
                    Error = "Wrong email or Password";
                }
            });
            RegistrationCommand = new Command((x) =>
            {
                listener.openRegistrationView();
            });
        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public string Error { get { return _error; } set
            {
                _error = value;
                OnPropertyChanged(nameof(Error));
            }
            }

        public ICommand LoginCommand { get; }
        public ICommand RegistrationCommand { get; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public interface OnOpenMainView
    {
        void openMainView();
        void openRegistrationView();
    }
}
