﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [Serializable]
    public class ResponseObject
    {
        public Status status { get; set; }
        public User user { get; set; }
        public List<Model.MainDish> MainDishes { get; set; }
        public List<CategoryDish> CategoryDishes { get; set; }
    }
}
