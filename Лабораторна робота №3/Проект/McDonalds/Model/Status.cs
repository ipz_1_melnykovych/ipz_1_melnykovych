﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [Serializable]
    public enum Status
    {
        LOGIN,
        REGISTRATION,
        MAIN_DISHES,
        CATEGORY_DISHES
    }
}
