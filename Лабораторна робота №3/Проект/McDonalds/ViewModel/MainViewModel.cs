﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Model;

namespace ViewModel
{
    public class MainViewModel : BaseViewModel
    {
        private MainViewNavigationListener _listener;
        private MainDishes _mainDish;
        private string _categoryDish;

        public MainViewModel(MainViewNavigationListener listener)
        {
            _listener = listener;

            FirstName = "First Name";
            LastName = "Last Name";
            HowManyFood = "0";
            Price = "$$$ Price $$$";
            
            SelectedCategoryDish = "Category Dishes";

            var dishes = DataManager.SelectMainDishes();
            dishes.ForEach(d => MainDishesObservableCollection.Add(d.name));

            LogoutCommand = new Command((o => _listener.OnOpenLoginViewListener()));
        }

        public ObservableCollection<string> MainDishesObservableCollection { get; set; } = new ObservableCollection<string>();
        public ObservableCollection<string> CategoryDishesObservableCollection { get; set; } = new ObservableCollection<string>();

        public ICommand LogoutCommand { get; }
        public ICommand OrderCommand { get; }
        public ICommand SaveCommand { get; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string HowManyFood { get; set; }
        public string Price { get; set; }

        public MainDishes SelectedMainDish
        {
            get { return _mainDish;}
            set
            {
                _mainDish = value;
                SetCategoryDishes();
            } }

        public string SelectedCategoryDish { get; set; }
        
        private void SetCategoryDishes()
        {
            switch (SelectedMainDish)
            {
                case MainDishes.DRINKS:
                {
                    var categoryDishes = DataManager.SelectCategoryDishes(1);
                    categoryDishes.ForEach(cd => CategoryDishesObservableCollection.Add(cd.name));
                    break;
                }
                case MainDishes.BURGERS:
                {
                    var categoryDishes = DataManager.SelectCategoryDishes(2);
                    categoryDishes.ForEach(cd => CategoryDishesObservableCollection.Add(cd.name));
                        break;
                }
                case MainDishes.MENUS:
                {
                    var categoryDishes = DataManager.SelectCategoryDishes(3);
                    categoryDishes.ForEach(cd => CategoryDishesObservableCollection.Add(cd.name));
                        break;
                }
            }

            OnPropertyChanged(nameof(CategoryDishesObservableCollection));
        }
    }

    public enum MainDishes
    {
        DRINKS,
        BURGERS,
        MENUS
    }

    public interface MainViewNavigationListener
    {
        void OnOpenLoginViewListener();
    }
}
