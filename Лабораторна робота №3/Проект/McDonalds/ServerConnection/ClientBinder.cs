﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using Model;

namespace ServerConnection
{
    public class ClientBinder
    {
        private static ClientBinder instance;

        private string serverIp = "localhost";
        private int port = 8080;

        public static ClientBinder GetInstance()
        {
            if (instance == null)
            {
                instance = new ClientBinder();
            }

            return instance;
        }

        private ClientBinder()
        {
            
        }

        public List<Model.MainDish> SelectMainDishes()
        {
            List<Model.MainDish> dishes = null;
            try
            {
                TcpClient msgClient = new TcpClient(serverIp, port);
                NetworkStream msgStream = msgClient.GetStream();

                SendingObject sending = new SendingObject
                {
                    status = Status.MAIN_DISHES
                };
                byte[] msgData = ToByteArray(sending);

                msgStream.Write(msgData, 0, msgData.Length);

                byte[] responseData = new byte[msgClient.ReceiveBufferSize];
                msgStream.Read(responseData, 0, responseData.Length);
                ResponseObject response = FromByteArray<ResponseObject>(responseData);
                dishes = response.MainDishes;
                msgStream.Close();
                msgClient.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return dishes;
        }

        public List<Model.CategoryDish> SelectCategoryDishes(int mainDishId)
        {
            List<Model.CategoryDish> dishes = null;
            try
            {
                TcpClient msgClient = new TcpClient(serverIp, port);
                NetworkStream msgStream = msgClient.GetStream();

                SendingObject sending = new SendingObject
                {
                    status = Status.CATEGORY_DISHES,
                    mainDishId = mainDishId
                };
                byte[] msgData = ToByteArray(sending);

                msgStream.Write(msgData, 0, msgData.Length);

                byte[] responseData = new byte[msgClient.ReceiveBufferSize];
                msgStream.Read(responseData, 0, responseData.Length);
                ResponseObject response = FromByteArray<ResponseObject>(responseData);
                dishes = response.CategoryDishes;
                msgStream.Close();
                msgClient.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return dishes;
        }

        public List<Model.Order> SelectOrders()
        {
            //todo
            return null;
        }

        public List<Model.OrderedFood> SelectOrderedFood()
        {
            //todo
            return null;
        }

        public Model.User SelectUserByEmailAndPassword(string email, string password)
        {
            User user = null;
            try
            {
                TcpClient msgClient = new TcpClient(serverIp, port);
                NetworkStream msgStream = msgClient.GetStream();

                SendingObject sending = new SendingObject
                {
                    email = email,
                    password = password,
                    status = Status.LOGIN
                };
                byte[] msgData = ToByteArray(sending);

                msgStream.Write(msgData, 0, msgData.Length);

                byte[] responseData = new byte[msgClient.ReceiveBufferSize];
                msgStream.Read(responseData, 0, responseData.Length);
                ResponseObject response = FromByteArray<ResponseObject>(responseData);
                user = response.user;
                msgStream.Close();
                msgClient.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return user;
        }

        public void RemoveFoodFromCategoryDishes(int categoryDishId, int number)
        {
            //todo
        }

        public void CreateOrder(decimal sumPrice, bool eatIn)
        {
            //todo
        }

        public void CreateOrderedFood(List<Model.OrderedFood> orderedFood)
        {
            //todo
        }

        public void CreateUser(Model.User user)
        {
            try
            {
                TcpClient msgClient = new TcpClient(serverIp, port);
                SendingObject sending = new SendingObject {user = user, status = Status.REGISTRATION};
                byte[] msgData = ToByteArray(sending);

                NetworkStream msgStream = msgClient.GetStream();
                msgStream.Write(msgData, 0, msgData.Length);
                msgStream.Close();
                msgClient.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private T FromByteArray<T>(byte[] data)
        {
            if (data == null)
                return default(T);
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream(data))
            {
                object obj = bf.Deserialize(ms);
                return (T)obj;
            }
        }


        private byte[] ToByteArray<T>(T obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }
    }
}
